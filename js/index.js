
$(function(){


	var $site = $('#site');
	//获取指定坐标的方块
	function getCell(x,y){
		return getRow(y).find('.col').eq(x);
	}
	//获取一行方块
	function getRow(x){
		var row = $site.find('.row').eq(x);
		return row;
	}
	//获取一列方块
	function getCol(y){
		var rowSize = $site.find('.row').size();
		var colSize = $site.find('.row .col').size() / rowSize;
		var col = $site.find('.row .col').filter(function(index){
			if((index % colSize) == y){
				return true;
			}
		});
		return col;
	}

	function Cell(x,y,color,image){
		this.x = x;
		this.y = y;
		this.color = color;
		this.image = image;
		this.init = function(){
			
			getCell(this.x,this.y).css({'background-color':this.color});
		}
	}


	function Site(width,height,cell){
		this.width = width;
		this.height = height;
		var cont = new Array();
		for(var i = 0;i < width;i++){
			cont[i] = new Array();
			for(var j = 0;j < height;j++){
				cont[i][j] = cell;
			}
		}
		this.cont = cont;
		this.addBlock = function(block){
			for(var i = 0; i < block.cells.length;i++){
				var cell = block.cells[i];
				if(cell.x < 0 || cell.x >= this.cont.length 
					|| cell.y < 0 || cell.y >= this.cont[0].length){
					console.log('方块未注册');
					console.log(cell);
					continue;
				}
				this.cont[cell.x][cell.y] = block.cells[i];
			}

			return true;
		}
		this.actBlock = null;
		this.moveActBlock = function(){

		}
		//刷新场地
		this.refresh = function(){
			for(var i = 0; i < this.cont.length;i++){
				for(var j = 0; j < this.cont[i].length;j++){
					var cell = this.cont[i][j];
					if(this.cont[i][j] != null){
						getCell(i,j).css({'background-color':cell.color,'background-image':cell.image});
					}else{
						getCell(i,j).css({'background-color':'','background-image':''});
					}
				}
			}
			console.log(this.actBlock);
			if(this.actBlock != null){
				this.actBlock.show();
			}
		}
	}
	

	function moveDown(block){
		block.cells[0].y += 1;
		block.cells[1].y += 1;
		block.cells[2].y += 1;
		block.cells[3].y += 1;
		return block;
	}
	function moveLeft(block){
		block.cells[0].x -= 1;
		block.cells[1].x -= 1;
		block.cells[2].x -= 1;
		block.cells[3].x -= 1;
		return block;
	}
	function moveRight(block){
		block.cells[0].x += 1;
		block.cells[1].x += 1;
		block.cells[2].x += 1;
		block.cells[3].x += 1;
		return block;
	}
	function moveUp(block){
		block.cells[0].y -= 1;
		block.cells[1].y -= 1;
		block.cells[2].y -= 1;
		block.cells[3].y -= 1;	
		return block;
	}

	function BlockT(x,y,color,image){
		this.cells = new Array();
		this.cells[0] = new Cell(x,y,color,image);
		this.cells[1] = new Cell(x - 1,y,color,image);
		this.cells[2] = new Cell(x,y - 1,color,image);
		this.cells[3] = new Cell(x + 1,y,color,image);
		this.show = function(){
			this.cells[0].init();
			this.cells[1].init();
			this.cells[2].init();
			this.cells[3].init();
		}

	}
	function BlockI(x,y){
		
	}
	function BlockL(x,y){
		
	}
	function BlockO(x,y){
		
	}
	function BlockZ(x,y){
		
	}

	var site = new Site(15,30,null);
	site.refresh();
	var blockT = new BlockT(3,5,'#dd0');
	site.actBlock = blockT;
	
	site.refresh();
	console.log(site);
	
	$(window).keydown(function(event){
		switch(event.keyCode) {
			case 38: site.actBlock = moveUp(site.actBlock);
			break;
			case 40: site.actBlock = moveDown(site.actBlock);
			break;
			case 37: site.actBlock = moveLeft(site.actBlock);
			break;
			case 39: site.actBlock = moveRight(site.actBlock);
			break;
		}
		console.log(site.actBlock);
		site.refresh();
		// site.actBlock.show();
	});

});